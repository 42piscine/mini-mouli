/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lhumbert <marvin@42lausanne.ch>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/12 02:37:58 by lhumbert          #+#    #+#             */
/*   Updated: 2021/07/13 22:37:13 by lhumbert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

// Libs
#include <stdio.h>
#include <string.h>
#include "../print.h"

// Test functions
void	test(char *str, int exp);
void	print_res(int exp, int res);

// Exercise function
/* 
 * Return 1 if str only contains lowercase chars or is empty, 0 otherwise
 */
int	ft_str_is_lowercase(char *str);

// Write all tests here
int	main(void)
{
	test("quarantedeux", 1);
	test("az", 1);
	test("quarante\ndeux", 0);
	test("QuaranteDeux", 0);
	test("42", 0);
	test("42 c'est c00l wsh", 0);
	test("", 1);
}

// Write the test function
void	test(char *str, int exp)
{
	int	ret;

	printf("Testing with: ");
	print_str("%s\n", str, YEL);
	ret = ft_str_is_lowercase(str);
	print_res(ret, exp);
}

// Write the function that displays the result
void	print_res(int ret, int exp)
{
	print_int("Expecting: %d\n", exp, MAG);
	if (ret == exp)
		print("OK\n", GRN);
	else
		print_int("NOT OK, got: %d\n", ret, RED);
	printf("=============================================\n\n");
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lhumbert <marvin@42lausanne.ch>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/12 02:37:58 by lhumbert          #+#    #+#             */
/*   Updated: 2021/07/13 20:56:23 by lhumbert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

// Libs
#include <stdio.h>
#include <string.h>
#include "../print.h"

// Test functions
void	test(char *str, int exp);
void	print_res(int exp, int res);

// Exercise function
int	ft_str_is_numeric(char *str);

// Write all tests here
int	main(void)
{
	test("quarante deux c'est cool wsh", 0);
	test("42", 1);
	test("0123456789", 1);
	test("/", 0);
	test(":", 0);
	test("42 c'est c00l wsh", 0);
	test("", 1);
}

// Write the test function
/*
 * This test verifies that src, dest and ret are all equal to the src passed
 * in the first place.
 */
void	test(char *str, int exp)
{
	int	ret;

	printf("Testing with: ");
	print_str("%s\n", str, YEL);
	ret = ft_str_is_numeric(str);
	print_res(ret, exp);
}

// Write the function that displays the result
void	print_res(int ret, int exp)
{
	print_int("Expecting: %d\n", exp, MAG);
	if (ret == exp)
		print("OK\n", GRN);
	else
		print_int("NOT OK, got: %d\n", ret, RED);
	printf("=============================================\n\n");
}

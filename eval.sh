#!/usr/bin/env bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

if [ -n "$1" ]
then
	i=$1
else
	i=0
fi

exdirname ()
{
	DIR=ex
	NUM=$( printf "%02d" $1 )
	echo $DIR$NUM
}

aftertest ()
{
	ex=$i
	PS3='What do you want to do: '
	options=("Show files" "Edit files" "Next test" "Quit")
	select opt in "${options[@]}"
	do
		case $opt in
			"Show files")
				find ../$1 -type f -name "*.c" \
					-exec printf '\n\e[33;1m%s\e[0m\n' {} \; \
					-exec cat {} \;
				read -p"Press Enter to continue..." key; echo
				break
				;;
			"Edit files")
				vim ../$1/*.c
				;;
			"Next test")
				break
				;;
			#"Retry")
			#	break
			#	;;
			"Quit")
				exit 0
				break
				;;
			*) break;;
		esac
	done
	#echo $RESULT
	#if [ "$RESULT" = "4" ];
	#then
	#	echo "retry"
	#	echo $(( $ex - 1 ))
	#else
	#	echo "continue"
	#	echo $ex
	#fi
}

nextdir=$( exdirname $i )
while [ -d "./$nextdir" ]
do
	printf "\n\n"
	echo "##############################################"
	echo "##################### $nextdir #####################"
	cp ../$nextdir/*.c $nextdir
	cd $nextdir
	gcc  -Wall -Wextra -Werror ../*.c *.c && ./a.out
	sleep 1
	cd ..
	aftertest $nextdir
	i=$(( $i + 1 ))
	nextdir=$( exdirname $i )
done

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lhumbert <marvin@42lausanne.ch>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/13 19:22:18 by lhumbert          #+#    #+#             */
/*   Updated: 2021/07/13 19:28:06 by lhumbert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "print.h"

void	print(char *msg, char *color)
{
	printf("%s", color);
	printf("%s", msg);
	printf(RESET);
}

void	print_int(char *msg, int nb, char *color)
{
	printf("%s", color);
	printf(msg, nb);
	printf(RESET);
}

void	print_int2(char *msg, int a, int b, char *color)
{
	printf("%s", color);
	printf(msg, a, b);
	printf(RESET);
}

void	print_str(char *msg, char *str, char *color)
{
	printf("%s", color);
	printf(msg, str);
	printf(RESET);
}

void	print_aint(char *msg, int *arr, int size, char *color)
{
	int	i;

	printf("%s", msg);
	printf("%s[", color);
	i = 0;
	while (i < size)
	{
		if (i != 0)
			printf(", ");
		printf("%d", arr[i]);
		i++;
	}
	printf("]");
	printf(RESET);
}

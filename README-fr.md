# Mini-Mouli
Ce projet a pour but d'être une "moulinnette" collaborative pour les étudiants de 42.

Le but est de garder le code aussi simple que possible tout en étant puissant.
Il doit intercepter autant d'erreurs que possible et aider les étudiants à envoyer un code correcte à la vraie "moulinette".

# TL;DR
Va dans le répertoire de ton projet et ensuite:
```
git clone --branch c02 https://gitlab.com/42piscine/mini-mouli.git
cd mini-mouli
./eval.sh
```

## Pourquoi faire ça
Ça peut demander beaucoup de temps et engendrer des erreurs de devoir penser à tous les cas d'erreur possibles et écrire une fonction `main()` pour chaque exercice. Alors pourquoi pas collaborer et s'entraider. C'est mieux que chaque étudiant écrive quelques très bons tests plutôt que toujours éditer la fonction `main()` voir même ne pas tester du tout...

## Comment ça marche
Pour chaque "projet" (comme C01, C02, etc...) il y a une branche. Sur chaque branche, à la racine nous trouvons un script bash,
des fonctions d'aide et un répertoire par exercice dans lequel nous trouvons un fichier `main.c`.

Ce fichier `main.c` contient généralement 3 fonctions:

* `int main(void)`: exécute les tests avec différents arguments et le résultat attendu.
* `void test(args...)`: imprime les arguments de test et le résultat attendu, exécute la fonction, envoie les résultats réels et attendus à `print_res()`.
* `void print_res(args...)`: imprime le résultat du test et affiche le résultat obtenu le test a échoué.

Le script `eval.sh` est responsable de lancer les tests sur chaque exercice. Il fonctionne comme ça:

1. En partant de l'exercice `ex00`, copier le(s) fichier(s) vers le répertoire du même nom dans ce repo.
1. Afficher le numéro de l'exercice.
1. Compiler et exécuter le(s) fichier(s) de l'étudiant avec les tests.
1. Attendre que l'utilisateur appuie sur Enter.
1. Répéter jusqu'à ce qu'il n'y ait plus d'exercice présent.

The helper `print.c` and its associated header `print.h` are responsible of printing beautiful colored outputs using `printf()` and `ANSI colors`.
The colors are defined (with `#define`) in `print.h` as string constants, refer to this file to find the printable colors.
Currently, these functions are:

* `void print(char *msg, char *color)` Prints a litteral string.
* `void print_int(char *msg, int nb, char *color)` Prints a formatted string with an integer value.
* `void print_int2(char *msg, int nb1, int nb2, char *color)` Prints two formatted integers separated with "and".
* `void print_str(char *msg, char *str, char *color)` Prints a formatted string with a string value.
* `void print_aint(char *msg, int *arr, int size, char *color)` Prints an array with brackets and comas.

## How to collaborate
Firstly, either ask @loichu (lhumbert on 42 Slack) to give you the rights to create a branch, which he'll certainly do.
Or if you want, you can fork this repo and then make a Pull Request.

If you don't know what to do, the easiest way is to find an exercise that has no test yet and then:

1. Create a branch called `cXX-username` (replace XX by the project's number and username by your username.
1. Copy the `main.c` from another exercise.
1. Make sure to declare the prototype of the function(s) you want to test.
1. Write the three functions mentioned above.
1. Make it work, ask @loichu (lhumbert) if any question.

## Other enhancements
* More helpers.
* Print n args of a single type.
* Print n args of any type (like does `printf()`).
* Add option to run the test on a single exercise in the shell script.
* Ask the user if he wants to open the file in vim after the test.
* Manage tests on ouputs, maybe with colored word-based diffs (see ydiff).
* Vimscript to run a test inside vim.
* Wrap everything in a user installable package.
* Any more ideas ?

# Troubleshoot
## My main is executed instead of the provided one
All C files in the student's directory are copied to the test directory, thus if you already
have a `main.c` file, it will override the provided one. Rename it, delete mini-mouli and try again. 

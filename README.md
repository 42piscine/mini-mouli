# Mini-Mouli
This project is intended to be a collaborative "moulinnette" for 42 students.

The goal is to keep the code as simple as possible, yet powerful.
It has to catch as many errors as possible and help the students to send a correct code to the real "moulinette".

# TL;DR
Go in your project directory and then:
```
git clone --branch c02 https://gitlab.com/42piscine/mini-mouli.git
cd mini-mouli
./eval.sh
```

## Why doing this
It can be very time consuming and error prone to think about all possible fail cases and to write a `main()` function for
every exercises. So why not collaborate and help each other. It is better if each student writes a few really good tests
than always editing the `main()` function or even not testing at all...

## How it works
For each "project" (like C01, C02, etc...) there is a branch. On each branch, at the root we can find a bash script,
some helper functions and one folder per exercise in which we find a `main.c` file.

This `main.c` file contains usually 3 functions:

* `int main(void)`: runs the tests with different args and the expected result.
* `void test(args...)`: outputs test args and expected result, runs the function, sends the actual and expected results to `print_res()`.
* `void print_res(args...)`: outputs the test result and shows actual result if failed.

The script `eval.sh` is responsible of running the tests on each exercise. It works like so:

1. Starting from exercise `ex00`, copy the student's C file(s) to the directory with the same name on this repo.
1. Print the number of the exercise.
1. Compile and execute the student's file(s) with the tests.
1. Wait for the user to press Enter.
1. Repeat until no more exercises are present.

The helper `print.c` and its associated header `print.h` are responsible of printing beautiful colored outputs using `printf()` and `ANSI colors`.
The colors are defined (with `#define`) in `print.h` as string constants, refer to this file to find the printable colors.
Currently, these functions are:

* `void print(char *msg, char *color)` Prints a litteral string.
* `void print_int(char *msg, int nb, char *color)` Prints a formatted string with an integer value.
* `void print_int2(char *msg, int nb1, int nb2, char *color)` Prints two formatted integers separated with "and".
* `void print_str(char *msg, char *str, char *color)` Prints a formatted string with a string value.
* `void print_aint(char *msg, int *arr, int size, char *color)` Prints an array with brackets and comas.

## How to collaborate
Firstly, either ask @loichu (lhumbert on 42 Slack) to give you the rights to create a branch, which he'll certainly do.
Or if you want, you can fork this repo and then make a Pull Request.

If you don't know what to do, the easiest way is to find an exercise that has no test yet and then:

1. Create a branch called `cXX-username` (replace XX by the project's number and username by your username.
1. Copy the `main.c` from another exercise.
1. Make sure to declare the prototype of the function(s) you want to test.
1. Write the three functions mentioned above.
1. Make it work, ask @loichu (lhumbert) if any question.

## Other enhancements
* More helpers.
* Print n args of a single type.
* Print n args of any type (like does `printf()`).
* Add option to run the test on a single exercise in the shell script.
* Ask the user if he wants to open the file in vim after the test.
* Manage tests on ouputs, maybe with colored word-based diffs (see ydiff).
* Vimscript to run a test inside vim.
* Wrap everything in a user installable package.
* Any more ideas ?

# Troubleshoot
## My main is executed instead of the provided one
All C files in the student's directory are copied to the test directory, thus if you already
have a `main.c` file, it will override the provided one. Rename it, delete mini-mouli and try again. 

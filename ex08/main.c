/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lhumbert <marvin@42lausanne.ch>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/12 02:37:58 by lhumbert          #+#    #+#             */
/*   Updated: 2021/07/13 21:04:16 by lhumbert         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

// Libs
#include <stdio.h>
#include <string.h>
#include "../print.h"

// Test functions
void	test(char *str, char *exp);
void	print_res(char *exp, char *res);

// Exercise function
char	*ft_strlowcase(char *str);

// Write all tests here
int	main(void)
{
	char	test_val1[] = "\x01\x05\x0a\x15";
	char	test_val2[] = "42\nC'EST COOL";
	char	test_val3[] = "42";
	char	test_val4[] = "42 C C00L +\"\\/~^";
	char	test_val5[] = "42 C'EST C00L WSH";
	char	test_val6[] = "";
	test(test_val1, "\x01\x05\x0a\x15");
	test(test_val2, "42\nc'est cool");
	test(test_val3, "42");
	test(test_val4, "42 c c00l +\"\\/~^");
	test(test_val5, "42 c'est c00l wsh");
	test(test_val6, "");
}

// Write the test function
void	test(char *str, char *exp)
{
	printf("Testing with: ");
	print_str("%s\n", str, YEL);
	print_res(ft_strlowcase(str), exp);
}

// Write the function that displays the result
void	print_res(char *ret, char *exp)
{
	print_str("Expecting: %s\n", exp, MAG);
	if (strcmp(ret, exp) == 0)
		print("OK\n", GRN);
	else
		print_str("NOT OK, got: %s\n", ret, RED);
	printf("=============================================\n\n");
}

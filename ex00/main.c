/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lhumbert <marvin@42lausanne.ch>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/12 02:37:58 by lhumbert          #+#    #+#             */
/*   Updated: 2021/07/14 22:31:36 by tpauvret         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

// Libs
#include <stdio.h>
#include <string.h>
#include "../print.h"

// Test functions
void	test(char *src);
void	print_res(char *dest, char *ret, char *exp);

// Exercise function
/* 
 * strcpy copies the source into the destination and returns the destination.
 */
char	*ft_strcpy(char *dest, char *src);

// Write all tests here
int	main(void)
{
	char	test_val[] = "42 c'est cool wsh";
	test(test_val);
}

// Write the test function
/*
 * This test verifies that src, dest and ret are all equal to the src passed
 * in the first place.
 */
void	test(char *src)
{
	char    dest[] = "12345678901234567";
	char    ft_dest[] = "12345678901234567";
	char    *exp = strcpy(dest, src);
	char    *ft_ret = ft_strcpy(ft_dest, src);

	
	printf("Testing with: ");
	print_str("%s\n", src, YEL);
	print_res(ft_dest, ft_ret, exp);
}

// Write the function that displays the result
void	print_res(char *dest, char *ret, char *exp)
{
	printf("Expecting: ");
	print_str("%s\n", exp, MAG);
	if (strcmp(exp, ret) == 0 && strcmp(exp, dest) == 0)
		print("OK\n", GRN);
	else
	{
		print_str("NOT OK, got: dest=%s ", dest, RED);
		print_str("and ret=%s\n", ret, RED);
	}
	printf("=============================================\n\n");
}
